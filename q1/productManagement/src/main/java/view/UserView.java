package view;

import java.text.ParseException;
import java.util.Scanner;

import controller.ProductController;
import controller.UserController;
import entity.User;

public class UserView {
	
	UserController userController;
	
	ProductController productController;
	
	ProductView productView;
	
	public void login(Scanner scanner) throws Exception {
		
		System.out.println("\n" );

		System.out.println("Welcome to this product management application !" );
		
		System.out.println("Please login");
		
		System.out.println("Enter your email : ");
				
		String email = scanner.nextLine();

		System.out.println("Enter your password : ");
		
		String password = scanner.nextLine();
		
		
		if (userController.IsUser(email, password)) {
			
			productController.setUserController(userController);
			
			this.menu(scanner);

		
		}
		else {
			
			System.out.print("incorrect email and/or password(s)");
			this.login(scanner);

		}
	}
	
	public void menu(Scanner scanner) throws Exception {
		

		System.out.println("Choose between 1, 2, 3, 4, 5 and 6");
		System.out.println("1 : To display products.");
		System.out.println("2 : To fetch a product by its ID.");
		System.out.println("3 : To add a new product.");
		System.out.println("4 : To delete a product by its ID.");
		System.out.println("5 : To update a product’s info.");
		System.out.println("6 : To log out.");
		
		int choice = scanner.nextInt();
		
		scanner.nextLine();
		
		if (choice == 1) {
			
			productView.getAllProduct();
			this.menu(scanner);
		}
		if (choice == 2) {
					
			productView.fetchProductByID(scanner);
			this.menu(scanner);

		}
		if (choice == 3) {
			productView.createProduct(scanner);
			this.menu(scanner);

			
		}
		if (choice == 4) {
			productView.removeProduct(scanner);
			this.menu(scanner);

		}
		if (choice == 5) {
			productView.updateProduct(scanner);
			this.menu(scanner);

		}
		if (choice == 6) {
			login(scanner);

		}
		
	}

	public UserController getUserController() {
		return userController;
	}

	public void setUserController(UserController userController) {
		this.userController = userController;
	}

	public ProductView getProductView() {
		return productView;
	}

	public void setProductView(ProductView productView) {
		this.productView = productView;
	}

	public ProductController getProductController() {
		return productController;
	}

	public void setProductController(ProductController productController) {
		this.productController = productController;
	}
	
	
}
