package view;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.FileHandler;


import org.apache.commons.cli.ParseException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import controller.ProductController;

import controller.UserController;
import entity.Product;
import entity.User;
import repository.ProductRepository;
import repository.UserRepository;
import view.ProductView;
import view.UserView;


@ComponentScan("controller")
@EntityScan("entity")
@EnableJpaRepositories("repository")
@SpringBootApplication(scanBasePackages = "controller")

public class App {

	static UserRepository userRepository;

	static ProductRepository productRepository;

	static ProductController productController;

	static UserController userController;

	static ProductView productView;

	static UserView userView;
	
	private static FileHandler fileHandler;
	
	private static String logFile;

	private static String jsonFileRP;

	private static String jsonFileWP;
	
	private static String jsonFileMEPP;


	public static void main(String[] args) throws Exception {

		ApplicationContext ctx = SpringApplication.run(App.class, args);

		initElements(ctx);

		initUser();

		initProduct();

		Scanner scanner = new Scanner(System.in);


		userView.login(scanner);

		scanner.close();

	}

	public static void initElements(ApplicationContext ctx) {

		try {

			userRepository = ctx.getBean(UserRepository.class);

			productRepository = ctx.getBean(ProductRepository.class);

			productController = ctx.getBean(ProductController.class);

			userController = ctx.getBean(UserController.class);

			productController.setProductRepository(productRepository);
			
			productController.setUserController(userController);

			userController.setUserRepository(userRepository);

			productView = new ProductView();

			productView.setProductController(productController);

			userView = new UserView();

			userView.setProductView(productView);

			userView.setUserController(userController);
			
			userView.setProductController(productController);
		

		}

		catch (Exception e) {

		}

	}

	public static void initUser() {

		User user1 = new User("idu1",23,"ramladhmine@gmail.com","1122");
		User user2 = new User("idu2",23,"abdelmalik.mariem@gmail.com","3344");
		
		
		userRepository.save(user1);
		userRepository.save(user2);
	
	}
	

	public static void initProduct() throws Exception {

		Product product1 = new Product("idp1","pn1",(double) 3000,"10-10-2025");
		Product product2 = new Product("idp2","pn2",(double) 5000,"04-05-2026");

		
		productController.createProduct(product1);
		productController.createProduct(product2);

	}
	
	
}
