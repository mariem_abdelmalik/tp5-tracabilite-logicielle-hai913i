package entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.validator.constraints.Length;

@Entity
public class Product implements Comparable<Product> {

	@Id
	@Length(max = 100)
	private String ID;
	@Length(max = 100)
	private String name;
	private Double price;
	@Length(max = 100)
	private String expirationDate;

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Product(String iD, String name, Double price, String expirationDate) {
		super();
		ID = iD;
		this.name = name;
		this.price = price;
		this.expirationDate = expirationDate;
	}

	public Product() {
		super();
	}

	@Override
	public String toString() {

		return "product:{ id =" + this.ID + ", name =" + this.name + ", price =" + this.price + ", expirationDate ="
				+ this.expirationDate + " }";

	}

	@Override
	public int compareTo(Product p) {

		if (this.price == p.getPrice())
			
			return 0;

		if (this.price < p.getPrice())
			
			return 1;

		return -1;
	}

}
