package controller;

import java.util.List;
import java.util.Optional;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

//import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;

import entity.Product;
import exceptionHandling.AProductWithTheSameIDAlreadyExists;
import exceptionHandling.NoProductWithTheProvidedIDExists;
import repository.ProductRepository;

@Controller
public class ProductController {

	ProductRepository productRepository;

	
	FileHandler fileHandler;
	
	UserController userController;

	public List<Product> getProducts() throws Exception {

		String message = "INTERNAL SERVER ERROR";

		try {
			List<Product> products = productRepository.findAll();
			
			return products;

		} catch (Exception e) {

			throw new Exception(message);

		}
	}

	public Product getProductById(String id) throws Exception {

		String message = "INTERNAL SERVER ERROR";

		try {

			Optional<Product> optionalProduct = productRepository.findById(id);

			if (optionalProduct.isEmpty()) {

				message = "no product with the provided ID exists";


				throw new NoProductWithTheProvidedIDExists(message);

			} else {

				Product product = optionalProduct.get();

				return product;

			}
		} catch (NoProductWithTheProvidedIDExists e) {

			throw new NoProductWithTheProvidedIDExists(message);
		}
	}

	public void deleteProductById(String id) throws Exception {

		String message = "INTERNAL SERVER ERROR";

		try {

			if (productRepository.existsById(id)) {
				
				getProductRepository().deleteById(id);


			} else {

				message = "attempt to delete a product failed, no product with the provided ID exists";

				throw new NoProductWithTheProvidedIDExists(message);
			}
		} catch (Exception e) {

			throw new Exception(e);
		}
	}

	public void createProduct(Product product) throws Exception {

		String message = "INTERNAL SERVER ERROR";

		try {

			if (!productRepository.existsById(product.getID())) {

				productRepository.save(product);
				


			} else {

				message = "attempt to create new product failed, a product with the provided ID exists";


				throw new AProductWithTheSameIDAlreadyExists(message);
			}

		}catch (AProductWithTheSameIDAlreadyExists e) {

			throw new AProductWithTheSameIDAlreadyExists(message);
		}
	}

	public void updateProduct(Product product) throws Exception {

		String message = "INTERNAL SERVER ERROR";

		try {

			if (productRepository.existsById(product.getID())) {

				productRepository.save(product);


			} else {

				message = "attempt to update product failed, no product with the provided ID exists";

				throw new NoProductWithTheProvidedIDExists(message);

			}
		} catch (Exception e) {

			throw new Exception(message);
		}
	}

	public ProductRepository getProductRepository() {
		return productRepository;
	}

	public void setProductRepository(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	public FileHandler getFileHandler() {
		return fileHandler;
	}

	public void setFileHandler(FileHandler fileHandler) {
		this.fileHandler = fileHandler;
	}

	public UserController getUserController() {
		return userController;
	}

	public void setUserController(UserController userController) {
		this.userController = userController;
	}
	
	
	

}
