package profileMagement;

import java.util.HashMap;
import java.util.List;

import entity.MethodInfo;
import entity.Product;
import entity.User;

public class WriteOperationsProfile {

	private HashMap<User,  List<MethodInfo>> userInvocations = new HashMap<User,List<MethodInfo>>();

	private HashMap<User, List<Integer>> nbInvocationsByUser = new HashMap<User, List<Integer>>();

	public HashMap<User, List<MethodInfo>> getUserInvocations() {
		return userInvocations;
	}

	public void setUserInvocations(HashMap<User, List<MethodInfo>> userInvocations) {
		this.userInvocations = userInvocations;
	}

	public HashMap<User, List<Integer>> getNbInvocationsByUser() {
		return nbInvocationsByUser;
	}

	public void setNbInvocationsByUser(HashMap<User, List<Integer>> nbInvocationsByUser) {
		this.nbInvocationsByUser = nbInvocationsByUser;
	}


}
