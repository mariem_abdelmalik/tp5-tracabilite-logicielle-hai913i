package profileMagement;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import entity.MethodInfo;
import entity.Product;
import entity.User;
import repository.ProductRepository;

public class CreateProfiles {

	/* l'objet nbInvocationsByUser est utilisé avoir les informations des differents appel pour tous les utilisateurs*/
	HashMap<User, List<MethodInfo>> userInvocations = new HashMap<User, List<MethodInfo>>();
	
	/* l'objet nbInvocationsByUser est utilisé pour qu'on associe a chaque utilisateur le nombre d'appel de chaque méthode
	 * , la position 0 contiendra le nb d'appel de get Allproducts, 1 pour getProductById, 2 pour createProduct, 
	 * 3 pour updateProduct et 4 pour deleteProductById*/
	
	HashMap<User, List<Integer>> nbInvocationsByUser = new HashMap<User, List<Integer>>();

	ReadOperationsProfile readOperationsProfile;

	WriteOperationsProfile writeOperationsProfile;

	MostExpensiveProductsProfile mostExpensiveProductsProfile = new MostExpensiveProductsProfile();

	ProductRepository productRepository;

	List<Product> mostExpensiveProducts = new ArrayList<Product>();

	/*pour remplir un objet json associer à l'ensemle des utilisateurs pour un profils donnée */
	public JSONArray getUserJson(JSONObject itemUser, HashMap<User, List<MethodInfo>> userInvocations,
			HashMap<User, List<Integer>> nbInvocationsByUser, JSONArray userJson) {

		for (User user : userInvocations.keySet()) {

			JSONArray methods = new JSONArray();
			JSONObject item = new JSONObject();

			itemUser.put("ID", user.getID());
			itemUser.put("Age", user.getAge());
			itemUser.put("Email", user.getEmail());
			itemUser.put("Password", user.getPassword());

			JSONArray methodInfoArray = new JSONArray();
			JSONObject item2 = new JSONObject();

			for (MethodInfo methodInfo : userInvocations.get(user)) {

				if (methodInfo.getName().equals("getProducts")) {
					for (Map.Entry<String, Product> set2 : methodInfo.getDateProduct().entrySet()) {

						item2.put("Timestamp", set2.getKey());

						methodInfoArray.add(item2);
						item2 = new JSONObject();
					}

					item.put("Timestamp for each call", methodInfoArray);

					methodInfoArray = new JSONArray();

					item.put("name", "getProducts");
					item.put("call number", nbInvocationsByUser.get(user).get(0));

					methods.add(item);
					item = new JSONObject();

				} else if (methodInfo.getName().equals("getProductById")) {

					for (Map.Entry<String, Product> set2 : methodInfo.getDateProduct().entrySet()) {

						item2.put("Timestamp", set2.getKey());
						item2.put("the desired product", set2.getValue());

						methodInfoArray.add(item2);
						item2 = new JSONObject();
					}
					item.put("method info", methodInfoArray);

					methodInfoArray = new JSONArray();

					item.put("name", "getProductById");
					item.put("call number", nbInvocationsByUser.get(user).get(1));

					methods.add(item);
					item = new JSONObject();

				} else if (methodInfo.getName().equals("createProduct")) {

					for (Map.Entry<String, Product> set2 : methodInfo.getDateProduct().entrySet()) {

						item2.put("Timestamp", set2.getKey());
						item2.put("the created product", set2.getValue());

						methodInfoArray.add(item2);
						item2 = new JSONObject();
					}

					item.put("method info", methodInfoArray);

					methodInfoArray = new JSONArray();

					item.put("name", "createProduct");
					item.put("call number", nbInvocationsByUser.get(user).get(2));

					methods.add(item);
					item = new JSONObject();

				} else if (methodInfo.getName().equals("updateProduct")) {

					for (Map.Entry<String, Product> set2 : methodInfo.getDateProduct().entrySet()) {

						item2.put("Timestamp", set2.getKey());
						item2.put("the updated product", set2.getValue());

						methodInfoArray.add(item2);
						item2 = new JSONObject();
					}
					item.put("method info", methodInfoArray);

					methodInfoArray = new JSONArray();

					item.put("name", "updateProduct");
					item.put("call number", nbInvocationsByUser.get(user).get(3));

					methods.add(item);
					item = new JSONObject();

				} else if (methodInfo.getName().equals("deleteProductById")) {

					for (Map.Entry<String, Product> set2 : methodInfo.getDateProduct().entrySet()) {

						item2.put("Timestamp", set2.getKey());
						item2.put("the deleted product", set2.getValue());

						methodInfoArray.add(item2);
						item2 = new JSONObject();
					}
					item.put("method info", methodInfoArray);

					methodInfoArray = new JSONArray();

					item.put("name", "deleteProductById");
					item.put("call number", nbInvocationsByUser.get(user).get(4));

					methods.add(item);
					item = new JSONObject();

				}

			}
			itemUser.put("methods", methods);
			userJson.add(itemUser);
			itemUser = new JSONObject();

		}

		return userJson;
	}

	/*creation des differents fichiers jsons pour les 3 profils*/
	
	public void generateJsonFile(String jsonFileReadProfile, String jsonFileWriteProfile, String jsonFileMEPP)
			throws Exception {

		JSONObject jsonObjectReadOperationsProfile = new JSONObject();

		JSONObject jsonObjectWriteOperationsProfile = new JSONObject();

		JSONObject jsonObjectMostExpensiveProductsProfile = new JSONObject();

		JSONArray usersRP = new JSONArray();

		JSONArray usersWP = new JSONArray();

		JSONArray usersMEPP = new JSONArray();

		JSONObject itemUser = new JSONObject();

		usersRP = this.getUserJson(itemUser, this.readOperationsProfile.getUserInvocations(),
				this.readOperationsProfile.getNbInvocationsByUser(), usersRP);
		jsonObjectReadOperationsProfile.put("users", usersRP);
		itemUser = new JSONObject();

		usersWP = this.getUserJson(itemUser, this.writeOperationsProfile.getUserInvocations(),
				this.writeOperationsProfile.getNbInvocationsByUser(), usersWP);

		jsonObjectWriteOperationsProfile.put("users", usersWP);
		itemUser = new JSONObject();

		List<Product> mostExpensiveProducts = new ArrayList<Product>();

		mostExpensiveProducts = this.getMostExpensiveProduct();

		usersMEPP = this.getUserJson(itemUser, this.mostExpensiveProductsProfile.getUserInvocations(),
				this.mostExpensiveProductsProfile.getNbInvocationsByUser(), usersMEPP);
		jsonObjectMostExpensiveProductsProfile.put("users", usersMEPP);

		try {
			FileWriter fileReadProfile = new FileWriter(jsonFileReadProfile);
			FileWriter fileWriteProfile = new FileWriter(jsonFileWriteProfile);
			FileWriter fileExpensivesProductsProfile = new FileWriter(jsonFileMEPP);

			fileReadProfile.write(jsonObjectReadOperationsProfile.toJSONString());
			fileWriteProfile.write(jsonObjectWriteOperationsProfile.toJSONString());
			fileExpensivesProductsProfile.write(jsonObjectMostExpensiveProductsProfile.toJSONString());

			fileReadProfile.close();
			fileWriteProfile.close();
			fileExpensivesProductsProfile.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*parsage du fichier de log pour pouvoir generer des profils */
	public void parser(File logfile) {

		try {

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();

			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

			Document doc = dBuilder.parse(logfile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("record");

			mostExpensiveProducts = this.getMostExpensiveProduct();

			for (int i = 0; i < nList.getLength(); i++) {

				Node nNode = nList.item(i);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					String methode = eElement.getElementsByTagName("method").item(0).getTextContent().toString();

					if (methode.equals("getProducts") || methode.equals("getProductById")
							|| methode.equals("createProduct") || methode.equals("updateProduct")
							|| methode.equals("deleteProductById")) {

						User user = new User();

						user = this.buildUser(eElement);

						if (methode.equals("getProducts")) {

							User user2 = this.checkIfUserInvocationsContainsUser(this.userInvocations, user);

							if (!(user2 == null)) {

								this.nbInvocationsByUser.get(user2).set(0,
										this.nbInvocationsByUser.get(user2).get(0) + 1);

								MethodInfo methodInfo = new MethodInfo();
								methodInfo.setName("getProducts");

								methodInfo.getDateProduct().put(
										eElement.getElementsByTagName("date").item(0).getTextContent(),
										this.buildProduct(eElement));

								this.userInvocations.get(user2).add(methodInfo);

							} else {

								List<Integer> l = new ArrayList<Integer>(Collections.nCopies(5, 0));

								HashMap<String, MethodInfo> hashMap = new HashMap<String, MethodInfo>();

								List<MethodInfo> methodInfos = new ArrayList<MethodInfo>();

								MethodInfo methodInfo = new MethodInfo();

								methodInfos.add(methodInfo);

								methodInfo.setName("getProducts");

								methodInfo.getDateProduct().put(
										eElement.getElementsByTagName("date").item(0).getTextContent(),
										this.buildProduct(eElement));

								l.set(0, 1);

								this.nbInvocationsByUser.put(user, l);

								this.userInvocations.put(user, methodInfos);
							}
						}

						else if (methode.equals("getProductById")) {

							this.initAttributes(user, 1, "getProductById", eElement, userInvocations,
									nbInvocationsByUser);

						}

						else if (methode.equals("createProduct")) {

							this.initAttributes(user, 2, "createProduct", eElement, userInvocations,
									nbInvocationsByUser);

						}

						else if (methode.equals("updateProduct")) {

							this.initAttributes(user, 3, "updateProduct", eElement, userInvocations,
									nbInvocationsByUser);
						}

						else if (methode.equals("deleteProductById")) {

							this.initAttributes(user, 4, "deleteProductById", eElement, userInvocations,
									nbInvocationsByUser);

						}

					}

				}

			}

		}

		catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void initProfiles() throws Exception {

		readOperationsProfile = new ReadOperationsProfile();

		writeOperationsProfile = new WriteOperationsProfile();

		List<Integer> nbInvocations = new ArrayList<Integer>();

		mostExpensiveProducts = this.getMostExpensiveProduct();

		int nbOfReadOperations = 0;

		int nbOfWriteOperations = 0;

		for (User user : this.nbInvocationsByUser.keySet()) {

			nbInvocations = this.nbInvocationsByUser.get(user);

			nbOfReadOperations = nbInvocations.get(0) + nbInvocations.get(1);

			nbOfWriteOperations = nbInvocations.get(2) + nbInvocations.get(3) + nbInvocations.get(4);

			if (nbOfReadOperations > nbOfWriteOperations) {

				this.readOperationsProfile.getNbInvocationsByUser().put(user, this.nbInvocationsByUser.get(user));

				this.readOperationsProfile.getUserInvocations().put(user, this.userInvocations.get(user));

			} else if (nbOfReadOperations < nbOfWriteOperations) {

				this.writeOperationsProfile.getNbInvocationsByUser().put(user, this.nbInvocationsByUser.get(user));

				this.writeOperationsProfile.getUserInvocations().put(user, this.userInvocations.get(user));

			}

		}
	}
	

	/*recuperation des 5 produits les plus chers dans la BDD*/
	public List<Product> getMostExpensiveProduct() throws Exception {

		List<Product> products = new ArrayList<Product>();

		products = this.productRepository.findAll();

		Collections.sort(products);

		products = products.stream().limit(5).collect(Collectors.toList());

		return products;

	}
	
	/*construction d'un objet utilisateur à partir du message du fichier de log*/
	public User buildUser(Element eElement) {

		String s = StringUtils.substringAfter(eElement.getElementsByTagName("message").item(0).getTextContent(),
				"user:{");

		String userinfo = StringUtils.substringBefore(s, " }");

		List<String> userinfoList = new ArrayList<String>(Arrays.asList(userinfo.split(",")));

		for (int j = 0; j < userinfoList.size(); j++) {

			userinfoList.set(j, StringUtils.substringAfter(userinfoList.get(j), "="));
		}

		User user = new User(userinfoList.get(0), Integer.parseInt(userinfoList.get(1)), userinfoList.get(2),
				userinfoList.get(3));

		return user;

	}

	/*construction d'un objet Product à partir du message du fichier de log*/
	public Product buildProduct(Element eElement) {
		String sp = StringUtils.substringAfter(eElement.getElementsByTagName("message").item(0).getTextContent(),
				"product:{");

		String productInfo = StringUtils.substringBefore(sp, " }");

		List<String> productInfoList = new ArrayList<String>(Arrays.asList(productInfo.split(",")));

		for (int j = 0; j < productInfoList.size(); j++) {

			productInfoList.set(j, StringUtils.substringAfter(productInfoList.get(j), "="));
		}

		Product product = new Product(productInfoList.get(0), productInfoList.get(1),
				Double.parseDouble(productInfoList.get(2)), productInfoList.get(3));

		return product;

	}

	/*Recuperation de la date à partir du date dans fichier de log*/
	public String buildDate(Element eElement) {

		String s = StringUtils.substringAfter(eElement.getElementsByTagName("date").item(0).getTextContent(), ">");

		String date = StringUtils.substringBefore(s, "<");

		return date;

	}

	public void initAttributes(User user, int position, String methodName, Element eElement,
			HashMap<User, List<MethodInfo>> userInvocations2, HashMap<User, List<Integer>> nbInvocationsByUser)
			throws Exception {

		Product product = new Product();
		product = this.buildProduct(eElement);

		if (methodName.equals("getProductById") && !(checkIfMostExpensiveProductContainsProduct(product) == null)) {

			this.initnbInvocationByUserAndUserInvocationsAndMostExpensiveProfile(user, position, methodName, eElement,
					this.mostExpensiveProductsProfile.getUserInvocations(),
					this.mostExpensiveProductsProfile.getNbInvocationsByUser());

		}

		this.initnbInvocationByUserAndUserInvocationsAndMostExpensiveProfile(user, position, methodName, eElement,
				this.userInvocations, this.nbInvocationsByUser);

	}

	/*
	 * initialisation de : nbInvocationByUser, UserInvocations et MostExpensiveProfile
	 * */
	public void initnbInvocationByUserAndUserInvocationsAndMostExpensiveProfile(User user, int position,
			String methodName, Element eElement, HashMap<User, List<MethodInfo>> userInvocations2,
			HashMap<User, List<Integer>> nbInvocationsByUser) {
		User user2 = this.checkIfUserInvocationsContainsUser(userInvocations2, user);
		if (!(user2 == null)) {

			nbInvocationsByUser.get(user2).set(position, nbInvocationsByUser.get(user2).get(position) + 1);

			Product product = new Product();

			product = this.buildProduct(eElement);

			MethodInfo methodInfo = this.checkIfuserInvocationsContainsMethod(userInvocations2, user2, methodName);

			if (!(methodInfo == null)) {

				userInvocations2.get(user2).remove(methodInfo);

				methodInfo.getDateProduct().put(eElement.getElementsByTagName("date").item(0).getTextContent(),
						product);

				userInvocations2.get(user2).add(methodInfo);

			} else {
				MethodInfo methodInfo1 = new MethodInfo();

				methodInfo1.setName(methodName);

				methodInfo1.getDateProduct().put(eElement.getElementsByTagName("date").item(0).getTextContent(),
						this.buildProduct(eElement));
				userInvocations2.get(user2).add(methodInfo1);

			}

		} else {

			List<Integer> l = new ArrayList<Integer>(Collections.nCopies(5, 0));

			List<MethodInfo> methodInfos = new ArrayList<MethodInfo>();

			MethodInfo methodInfo = new MethodInfo();

			methodInfo.setName(methodName);

			methodInfo.getDateProduct().put(eElement.getElementsByTagName("date").item(0).getTextContent(),
					this.buildProduct(eElement));

			methodInfos.add(methodInfo);

			l.set(position, 1);

			nbInvocationsByUser.put(user, l);

			userInvocations2.put(user, methodInfos);
		}

	}

	public User checkIfUserInvocationsContainsUser(HashMap<User, List<MethodInfo>> userInvocations2, User user) {

		for (User user2 : userInvocations2.keySet()) {

			if (user2.getID().equals(user.getID())) {

				return user2;
			}
		}
		return null;
	}

	public MethodInfo checkIfuserInvocationsContainsMethod(HashMap<User, List<MethodInfo>> userInvocations, User user,
			String methodName) {

		for (MethodInfo methodInfo : userInvocations.get(user)) {

			if (methodInfo.getName() == methodName) {
				return methodInfo;
			}

		}
		return null;
	}

	public Product checkIfMostExpensiveProductContainsProduct(Product product) {

		for (Product product2 : this.mostExpensiveProducts) {

			if (product2.getID().equals(product.getID())) {

				return product2;
			}
		}
		return null;
	}

	public HashMap<User, List<MethodInfo>> getUserInvocations() {
		return userInvocations;
	}

	public void setUserInvocations(HashMap<User, List<MethodInfo>> userInvocations) {
		this.userInvocations = userInvocations;
	}

	public List<Product> getMostExpensiveProducts() {
		return mostExpensiveProducts;
	}

	public void setMostExpensiveProducts(List<Product> mostExpensiveProducts) {
		this.mostExpensiveProducts = mostExpensiveProducts;
	}

	public HashMap<User, List<Integer>> getNbInvocationsByUser() {
		return nbInvocationsByUser;
	}

	public void setNbInvocationsByUser(HashMap<User, List<Integer>> nbInvocationsByUser) {
		this.nbInvocationsByUser = nbInvocationsByUser;
	}

	public ReadOperationsProfile getReadOperationsProfile() {
		return readOperationsProfile;
	}

	public void setReadOperationsProfile(ReadOperationsProfile readOperationsProfile) {
		this.readOperationsProfile = readOperationsProfile;
	}

	public WriteOperationsProfile getWriteOperationsProfile() {
		return writeOperationsProfile;
	}

	public void setWriteOperationsProfile(WriteOperationsProfile writeOperationsProfile) {
		this.writeOperationsProfile = writeOperationsProfile;
	}

	public MostExpensiveProductsProfile getMostExpensiveProductsProfile() {
		return mostExpensiveProductsProfile;
	}

	public void setMostExpensiveProductsProfile(MostExpensiveProductsProfile mostExpensiveProductsProfile) {
		this.mostExpensiveProductsProfile = mostExpensiveProductsProfile;
	}

	public ProductRepository getProductRepository() {
		return productRepository;
	}

	public void setProductRepository(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

}