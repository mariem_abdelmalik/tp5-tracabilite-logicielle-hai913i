package repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, String> {
	
}