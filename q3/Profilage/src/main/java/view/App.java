package view;

import java.io.File;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


import profileMagement.CreateProfiles;
import repository.ProductRepository;
import repository.UserRepository;


@EntityScan("entity")
@EnableJpaRepositories("repository")
@SpringBootApplication()
public class App {

	static UserRepository userRepository;

	static ProductRepository productRepository;

	private static String logFile;

	private static String jsonFileRP;

	private static String jsonFileWP;

	private static String jsonFileMEPP;

	private static CreateProfiles createProfiles;

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		ApplicationContext ctx = SpringApplication.run(App.class, args);


		userRepository = ctx.getBean(UserRepository.class);

		productRepository = ctx.getBean(ProductRepository.class);

		createProfiles = new CreateProfiles();
		
		createOptions(args);

			
		File file = new File(logFile);

		createProfiles.setProductRepository(productRepository);

		createProfiles.parser(file);

		createProfiles.initProfiles();

		createProfiles.generateJsonFile(jsonFileRP, jsonFileWP, jsonFileMEPP);

	}

	public static void createOptions(String[] args) {

		Options options = new Options();

		Option logFileOption = Option.builder("logFile").longOpt("logFile").argName("logFile").hasArg().required(true)
				.build();
		options.addOption(logFileOption);

		Option outputFileR = Option.builder("jsonFileRP").longOpt("jsonFileRP").argName("jsonFileRP").hasArg()
				.required(true).build();

		Option outputFileW = Option.builder("jsonFileWP").longOpt("jsonFileWP").argName("jsonFileWP").hasArg()
				.required(true).build();

		Option outputFileMEP = Option.builder("jsonFileMEPP").longOpt("jsonFileMEPP").argName("jsonFileMEPP").hasArg()
				.required(true).build();

		options.addOption(logFileOption);

		options.addOption(outputFileR);

		options.addOption(outputFileW);

		options.addOption(outputFileMEP);

		CommandLine cl;
		CommandLineParser clparser = new BasicParser();
		HelpFormatter helper = new HelpFormatter();

		try {

			cl = clparser.parse(options, args);
			if (cl.hasOption("logFile")) {
				logFile = cl.getOptionValue("logFile");
			}

			if (cl.hasOption("jsonFileRP")) {
				jsonFileRP = cl.getOptionValue("jsonFileRP");
			}
			if (cl.hasOption("jsonFileWP")) {
				jsonFileWP = cl.getOptionValue("jsonFileWP");
			}

			if (cl.hasOption("jsonFileMEPP")) {
				jsonFileMEPP = cl.getOptionValue("jsonFileMEPP");
			}
		} catch (ParseException e) {

			System.out.println(e.getMessage());
			helper.printHelp("Usage:", options);
			System.exit(0);
		}

	}

}
