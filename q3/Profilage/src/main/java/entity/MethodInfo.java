package entity;

import java.util.HashMap;
import java.util.List;

public class MethodInfo {


	// le nom de la methode
	private String name ;

	/* product représente le produit résultat de l'opération ou donné comme entre cela depend de la méthode et 
	ce string représente la date de l'appel. */
	private HashMap<String,Product> dateProduct = new HashMap<String,Product>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public HashMap<String, Product> getDateProduct() {
		return dateProduct;
	}

	public void setDateProduct(HashMap<String, Product> dateProduct) {
		this.dateProduct = dateProduct;
	}
	
	
}
	

	