package entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.validator.constraints.Length;

@Entity
public class User {

	@Id
    @Length(max = 100)
	private String id;
	private int age;
	@Length(max = 100)	
	private String email;
	@Length(max = 100)
	private String password;
	
	public User() {
		
	}
	
	public User(String iD, int age, String email, String password) {
		super();
		id = iD;
		this.age = age;
		this.email = email;
		this.password = password;
	}
	
	public String getID() {
		return id;
	}
	public void setID(String iD) {
		id = iD;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		
		return "user:{id ="+ this.id + ", age ="+this.age +", email ="+this.email+", password ="+this.password+" }";
				
	}
	@Override
    public boolean equals(Object o) {
 
        if (o == this) {
            return true;
        }
 
        if (!(o instanceof User)) {
            return false;
        }
         
        User u = (User) o;
         
        return u.getID().equals(this.getID());
    }
	
	
}
