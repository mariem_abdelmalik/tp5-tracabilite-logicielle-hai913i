package view;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.FileHandler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import controller.ProductController;


import controller.UserController;
import entity.Product;
import entity.User;
import repository.ProductRepository;
import repository.UserRepository;
import view.ProductView;
import view.UserView;
import java.util.logging.Logger;


@ComponentScan("controller")
@EntityScan("entity")
@EnableJpaRepositories("repository")
@SpringBootApplication(scanBasePackages = "controller")

public class App {

	static UserRepository userRepository;

	static ProductRepository productRepository;

	static ProductController productController;

	static UserController userController;

	static ProductView productView;

	static UserView userView;
	
	private static Logger LOGGER = Logger.getLogger(App.class.getName());

	private static FileHandler fileHandler;
	
	static {
		
		try {
			
			fileHandler = new FileHandler("App.log", true);
			
			LOGGER.addHandler(fileHandler);
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		LOGGER.info("initialization of different objects");

		ApplicationContext ctx = SpringApplication.run(App.class, args);

		initElements(ctx);


		Scanner scanner = new Scanner(System.in);

		LOGGER.info("the beginning of the interaction with the user.");

		userView.login(scanner);

		scanner.close();

		LOGGER.info("the end of interaction with the user.");

	}

	public static void initElements(ApplicationContext ctx) {

		try {

			userRepository = ctx.getBean(UserRepository.class);

			productRepository = ctx.getBean(ProductRepository.class);

			productController = ctx.getBean(ProductController.class);

			userController = ctx.getBean(UserController.class);

			productController.setProductRepository(productRepository);

			productController.setLOGGER(LOGGER);
			
			productController.setUserController(userController);

			userController.setUserRepository(userRepository);

			userController.setLOGGER(LOGGER);

			productView = new ProductView();

			productView.setProductController(productController);

			userView = new UserView();

			userView.setProductView(productView);

			userView.setUserController(userController);
			
			userView.setProductController(productController);
			
		}

		catch (Exception e) {

		}

	}

	

}
