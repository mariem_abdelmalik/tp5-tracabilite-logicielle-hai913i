package view;

import java.util.List;
import java.util.Scanner;

import javax.persistence.Column;
import javax.persistence.Id;

import org.springframework.beans.factory.annotation.Autowired;

import controller.ProductController;
import entity.Product;

public class ProductView {
	
    //@Autowired
	ProductController productController;
    
	public void getAllProduct() throws Exception {
		
		List<Product> products = productController.getProducts();
		
		if (!products.isEmpty()) {
			
			System.out.println("List of products : ");
			
			System.out.println("ID    Name  Price  Expiration Date");
			
			for (Product product : products) {
				
				System.out.println(product.getID()+"  "+product.getName()+"   "+product.getPrice()+" "+product.getExpirationDate());
				
			}
			System.out.println("\n");

		}
	}
	
	public void fetchProductByID(Scanner scanner) throws Exception {
		
		String ID;
		
		System.out.println("Enter the product identifier");
		
		ID = scanner.nextLine();

		Product product = productController.getProductById(ID);
		
		System.out.println("The desired product :");

		System.out.println("Product id : "+product.getID());
			
		System.out.println("Product name : "+product.getName());
			
		System.out.println("Product price : "+product.getPrice());
			
		System.out.println("Product ExpirationDate : "+product.getExpirationDate());
		
		System.out.println("\n");


	}

	
	public void createProduct(Scanner scanner) throws Exception {
		
		String attribute;
		Double price;
		
		Product product = new Product();
		
		System.out.println("Enter the product identifier");
		attribute = scanner.nextLine();
		product.setID(attribute);
		
		System.out.println("Enter the product name");
		attribute = scanner.nextLine();
		product.setName(attribute);

		System.out.println("Enter the expiration date");
		attribute = scanner.nextLine();
		product.setExpirationDate(attribute);
		

		System.out.println("Enter product price");
		price = (double) scanner.nextInt();
		product.setPrice(price);
		
		this.productController.createProduct(product);
		
		System.out.println("Product added successfully");
	
	}
	
	public void updateProduct(Scanner scanner) throws Exception {
		
		Product product = new Product();
		
		System.out.println("Enter the product identifier");
		String id;
		id = scanner.nextLine();
		product.setID(id);
		
		System.out.println("Enter the product name");
		String name;
		name = scanner.nextLine();
		product.setName(name);

		System.out.println("Enter the expiration date");
		String expirationDate;
		expirationDate = scanner.nextLine();
		product.setExpirationDate(expirationDate);
		
		System.out.println("Enter product price");
		Double price;
		price = (double) scanner.nextInt();
		product.setPrice(price);
		
		this.productController.updateProduct(product);
		
		System.out.println("Product updated successfully");

		
	}

	public void removeProduct(Scanner scanner) throws Exception {
		
		String ID;
		System.out.println("Enter the product identifier");
		ID = scanner.nextLine();

		this.productController.deleteProductById(ID);
		
		System.out.println("Product removed successfully");

	}
	
	
	public ProductController getProdutController() {
		return productController;
	}

	public void setProductController(ProductController productController) {
		this.productController = productController;
	}
	
	
	
	

}
