package controller;

import java.util.Optional;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import org.springframework.stereotype.Controller;

import entity.Product;
import entity.User;
import exceptionHandling.NoProductWithTheProvidedIDExists;
import repository.UserRepository;

@Controller
public class UserController {
	

	UserRepository userRepository;
	
	User Currentuser;
	
	Logger LOGGER;
	
	FileHandler fileHandler;

	public boolean IsUser(String email,String password) throws Exception {
				
		try {
			
			LOGGER.info("checking if a user with "+email+" as email exists");
						
			User user = userRepository.getUserByEmail(email);
			
			if (user == null) {
				
				return false;

			}
			else {
				
				LOGGER.info("checking if the user found has the given password");

				if (user.getPassword().equals(password)) {
					
					this.Currentuser = user;
					
					return true;
				}
				return false;

			}
		} catch (Exception e) {
			
			this.LOGGER.severe("INTERNAL SERVER ERROR");
			
	        throw new Exception();
		}
	}
	
	public void createUser(User user) throws Exception {
		
		String message="INTERNAL SERVER ERROR";

		try {
			
			if(!userRepository.existsById(user.getID())) {
								
				userRepository.save(user);
				
				LOGGER.info("the user with "+user.getEmail()+"as email has been added");

		}
			else {
				
				message= "attempt to create new user failed, a user with the provided ID exists";
				
		        throw new Exception();
			}
						
		} catch (Exception e) {
			
	        throw new Exception(message);
		}
	}
	
	public User getUserById(String id) throws Exception {

		String message="INTERNAL SERVER ERROR";

		try {

			Optional<User> optionalUser = userRepository.findById(id);

			if (optionalUser.isEmpty()) {
				
				message = "User not found";
				
		        throw new Exception(message);

			} else {

				User user = optionalUser.get();

				return user;

			}
		} catch (Exception e) {

	        throw new Exception(message);
		}
	}
	
	public User getCurrentuser() {
		return Currentuser;
	}

	public void setCurrentuser(User currentuser) {
		Currentuser = currentuser;
	}

	public Logger getLOGGER() {
		return LOGGER;
	}

	public void setLOGGER(Logger lOGGER) {
		LOGGER = lOGGER;
	}

	public UserRepository getUserRepository() {
		return userRepository;
	}


	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public FileHandler getFileHandler() {
		return fileHandler;
	}

	public void setFileHandler(FileHandler fileHandler) {
		this.fileHandler = fileHandler;
	}
	
	

}
