package controller;

import java.util.List;
import java.util.Optional;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import org.springframework.stereotype.Controller;

import entity.Product;
import exceptionHandling.AProductWithTheSameIDAlreadyExists;
import exceptionHandling.NoProductWithTheProvidedIDExists;
import repository.ProductRepository;

@Controller
public class ProductController {

	ProductRepository productRepository;

	Logger LOGGER;
	
	FileHandler fileHandler;
	
	UserController userController;

	public List<Product> getProducts() throws Exception {

		String message = "INTERNAL SERVER ERROR";

		try {
			List<Product> products = productRepository.findAll();
			
			LOGGER.info("getting all products by " +userController.getCurrentuser().toString());

			return products;

		} catch (Exception e) {

			LOGGER.info("error while trying to get all products");

			throw new Exception(message);

		}
	}

	public Product getProductById(String id) throws Exception {

		String message = "INTERNAL SERVER ERROR";

		try {

			Optional<Product> optionalProduct = productRepository.findById(id);

			if (optionalProduct.isEmpty()) {

				message = "no product with the provided ID exists";

				LOGGER.info("error caused by trying to get a product that not exist");

				throw new NoProductWithTheProvidedIDExists(message);

			} else {

				Product product = optionalProduct.get();

				LOGGER.info("getting the " + product.toString() + " by " +userController.getCurrentuser().toString());

				return product;

			}
		} catch (NoProductWithTheProvidedIDExists e) {

			throw new NoProductWithTheProvidedIDExists(message);
		}
	}

	public void deleteProductById(String id) throws Exception {

		String message = "INTERNAL SERVER ERROR";

		try {

			if (productRepository.existsById(id)) {
				
				Product product = this.getProductById(id);

				getProductRepository().deleteById(id);

				LOGGER.info("deleting the" + product.toString() +" by "+userController.getCurrentuser().toString());


			} else {

				message = "attempt to delete a product failed, no product with the provided ID exists";

				LOGGER.info("error caused by trying to delete a product that not exist");

				throw new NoProductWithTheProvidedIDExists(message);
			}
		} catch (Exception e) {

			LOGGER.info("the id of the product that couldn't be removed : " + id);

			throw new Exception(e);
		}
	}

	public void createProduct(Product product) throws Exception {

		String message = "INTERNAL SERVER ERROR";

		try {

			if (!productRepository.existsById(product.getID())) {

				productRepository.save(product);
				
				LOGGER.info("the " + product.toString() + " has been added by "+userController.getCurrentuser().toString());


			} else {

				message = "attempt to create new product failed, a product with the provided ID exists";

				LOGGER.info("error caused by trying to add the product " + product.getName() + "identified by "
						+ product.getID() + " that already exists");

				throw new AProductWithTheSameIDAlreadyExists(message);
			}

		} catch (Exception e) {

			throw new Exception(message);
		}
	}

	public void updateProduct(Product product) throws Exception {

		String message = "INTERNAL SERVER ERROR";

		try {

			if (productRepository.existsById(product.getID())) {

				productRepository.save(product);

				LOGGER.info("the " + product.toString() + " has been updated by "+userController.getCurrentuser().toString());


			} else {

				message = "attempt to update product failed, no product with the provided ID exists";

				LOGGER.severe("error caused by trying to update a product that not exist");

				throw new NoProductWithTheProvidedIDExists(message);

			}
		} catch (Exception e) {

			throw new Exception(message);
		}
	}

	public ProductRepository getProductRepository() {
		return productRepository;
	}

	public void setProductRepository(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	public Logger getLOGGER() {
		return LOGGER;
	}

	public void setLOGGER(Logger lOGGER) {
		LOGGER = lOGGER;
	}

	public FileHandler getFileHandler() {
		return fileHandler;
	}

	public void setFileHandler(FileHandler fileHandler) {
		this.fileHandler = fileHandler;
	}

	public UserController getUserController() {
		return userController;
	}

	public void setUserController(UserController userController) {
		this.userController = userController;
	}
	
	
	

}
