package exceptionHandling;

public class NoProductWithTheProvidedIDExists extends Exception {
	
	public NoProductWithTheProvidedIDExists(String messageError) {
		super(messageError);
	}
	

}
