package exceptionHandling;

public class AProductWithTheSameIDAlreadyExists extends Exception{

	public AProductWithTheSameIDAlreadyExists(String messageError) {
		super(messageError);
	}
}
